<?php 

require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new animal("Shaun");
echo " Name : " . $sheep->name;
echo "<br>";
echo " legs : " . $sheep->legs;
echo "<br>";
echo " Cold Blooded : " . $sheep->cold_blooded;
echo "<br><br>";

$kodok = new Ape("Buduk");
echo " Nama : " . $kodok->name;
echo "<br>";
echo " legs : " . $kodok->legs;
echo "<br>";
echo " Cold Blooded : " . $kodok->cold_blooded;
echo "<br>";
echo "Jump : " . $kodok->jump;
echo "<br><br>";

$sungkong = new Ape("Kera Sakti");
echo " Nama : " . $sungkong->name;
echo "<br>";
echo " legs : " . $sungkong->legs;
echo "<br>";
echo " Cold Blooded : " . $sungkong->cold_blooded;
echo "<br>";
echo "Jump : " . $sungkong->jump;
echo "<br><br>";


?>